echo "America/Mexico_City" > /etc/timezone;
export LC_ALL="en_US.UTF-8";
unlink /etc/localtime && dpkg-reconfigure -f noninteractive tzdata;

apt -y update; apt -y upgrade && apt-get -y autoremove && apt-get -y autoclean;