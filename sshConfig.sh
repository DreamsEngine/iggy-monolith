cp /etc/ssh/sshd_config /etc/ssh/sshd_config.original && chmod a-w /etc/ssh/sshd_config.original;

if [ -d $HOME/sshd_config ]
then 
    rm -rf sshd_config;
    git clone https://gitlab.com/DreamsEngine/sshd_config.git $HOME/sshd_config;
else
    git clone https://gitlab.com/DreamsEngine/sshd_config.git $HOME/sshd_config;
fi

mv $HOME/sshd_config/sshd_config /etc/ssh
chmod 664 /etc/ssh/sshd_config;
rm -rf $HOME/sshd_config;

sudo systemctl restart sshd.service
service sshd restart;

echo "====================================";
echo "Let's create SSH-KEY"; 
echo "====================================";

ssh-keygen -t ed25519 -C  "$servername"; 
eval "$(ssh-agent -s)"; 
