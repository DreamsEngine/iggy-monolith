if [ -d /usr/local/bin/antibody ]
then 
    rm -rf /usr/local/bin/antibody;
    rm -rf $(antibody home);
    curl -sfL git.io/antibody | sh -s - -b /usr/local/bin;
else
    curl -sfL git.io/antibody | sh -s - -b /usr/local/bin;
fi


echo "====================================";
echo "I will configure Antibody for you";  
echo "====================================";

if [ -f $HOME/.zshrc ]
then 
rm -rf $HOME/.zshrc;
rm -rf $HOME/.zsh_plugins.txt ;

echo "# OMZ Properties
robbyrussell/oh-my-zsh
robbyrussell/oh-my-zsh path:plugins/git
robbyrussell/oh-my-zsh path:plugins/git-flow
robbyrussell/oh-my-zsh path:plugins/brew
robbyrussell/oh-my-zsh path:plugins/history
robbyrussell/oh-my-zsh path:plugins/npm
robbyrussell/oh-my-zsh path:plugins/copyfile
robbyrussell/oh-my-zsh path:plugins/docker


# Third Party
zsh-users/zsh-autosuggestions
zsh-users/zsh-completions
zsh-users/zsh-completions src
zsh-users/zsh-history-substring-search
zsh-users/zsh-syntax-highlighting
zsh-users/zsh-autosuggestions
djui/alias-tips
caarlos0/zsh-open-github-pr
# danielbayerlein/zsh-plugins ls
mafredri/zsh-async
rupa/z
" >> $HOME/.zsh_plugins.txt;

echo "#~/.zshrc
export ZSH="$(antibody home)/https-COLON--SLASH--SLASH-github.com-SLASH-robbyrussell-SLASH-oh-my-zsh"

source <(antibody init)
antibody bundle < ~/.zsh_plugins.txt

# ZSH Theme
autoload -U promptinit; promptinit
prompt spaceship

source $HOME/.zsh_plugins.sh
source $HOME/.aliases
" >> $HOME/.zshrc;

rm -rf `antibody home`;
antibody bundle < $HOME/.zsh_plugins.txt > /dev/stdout;
source .zsh_plugins.sh


else
echo "# OMZ Properties
robbyrussell/oh-my-zsh
robbyrussell/oh-my-zsh path:plugins/git
robbyrussell/oh-my-zsh path:plugins/git-flow
robbyrussell/oh-my-zsh path:plugins/brew
robbyrussell/oh-my-zsh path:plugins/history
robbyrussell/oh-my-zsh path:plugins/npm
robbyrussell/oh-my-zsh path:plugins/copyfile
robbyrussell/oh-my-zsh path:plugins/docker


# Third Party
zsh-users/zsh-autosuggestions
zsh-users/zsh-completions
zsh-users/zsh-completions src
zsh-users/zsh-history-substring-search
zsh-users/zsh-syntax-highlighting
zsh-users/zsh-autosuggestions
djui/alias-tips
caarlos0/zsh-open-github-pr
# danielbayerlein/zsh-plugins ls
mafredri/zsh-async
rupa/z
" >> $HOME/.zsh_plugins.txt;

echo "#~/.zshrc
export ZSH="$(antibody home)/https-COLON--SLASH--SLASH-github.com-SLASH-robbyrussell-SLASH-oh-my-zsh"

source <(antibody init)
antibody bundle < ~/.zsh_plugins.txt

# ZSH Theme
autoload -U promptinit; promptinit
prompt spaceship

source $HOME/.zsh_plugins.sh
source $HOME/.aliases
" >> $HOME/.zshrc;

rm -rf `antibody home`;
antibody bundle < $HOME/.zsh_plugins.txt > /dev/stdout;
source .zsh_plugins.sh
fi


echo "====================================";
echo "Spaceship Theme Initialize";  
echo "====================================";


if [ -d $IGGYMONOLITH/.spaceship-prompt/ ]
then
rm -rf $IGGYMONOLITH/.spaceship-prompt/;
git clone https://github.com/denysdovhan/spaceship-prompt.git .spaceship-prompt;
mv .spaceship-prompt $IGGYMONOLITH;
ln -sf "$IGGYMONOLITH/.spaceship-prompt/spaceship.zsh" "/usr/local/share/zsh/site-functions/prompt_spaceship_setup";
mkdir -pv $HOME/.zfunctions/; 
ln -sf "$IGGYMONOLITH/.spaceship-prompt/spaceship.zsh" "$HOME/.zfunctions/prompt_spaceship_setup"

else
git clone https://github.com/denysdovhan/spaceship-prompt.git .spaceship-prompt;
mv .spaceship-prompt $IGGYMONOLITH;
ln -sf "$IGGYMONOLITH/.spaceship-prompt/spaceship.zsh" "/usr/local/share/zsh/site-functions/prompt_spaceship_setup";
mkdir -pv $HOME/.zfunctions/; 
ln -sf "$IGGYMONOLITH/.spaceship-prompt/spaceship.zsh" "$HOME/.zfunctions/prompt_spaceship_setup"
fi

antibody bundle < $HOME/.zsh_plugins.txt > $HOME/.zsh_plugins.sh

echo "====================================";
echo "Antibody Configurations Done"; 
echo "====================================";
