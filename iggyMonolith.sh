#!/bin/sh
export DEBIAN_FRONTEND=noninteractive;
export IGGYMONOLITH="$HOME/.dotfiles";
export servername=`hostname`;

echo "====================================";
echo "Hello";
echo "This is Iggy Autorave and I'm about to install your webserver";  
echo "====================================";

chmod +x $IGGYMONOLITH/*;

echo "====================================";
echo "Setting Time";  
echo "====================================";

$IGGYMONOLITH/setTime.sh;

echo "====================================";
echo "Installing Vim latest Version";  
echo "====================================";

$IGGYMONOLITH/installVim.sh;

echo "====================================";
echo "Installing Git";  
echo "====================================";

$IGGYMONOLITH/installGit.sh;

echo "====================================";
echo "Installing CURL";  
echo "====================================";

apt -y install curl;

echo "====================================";
echo "Installing ZSH";  
echo "====================================";

apt -y install zsh  && chsh -s /bin/zsh;

echo "====================================";
echo "Installing NODE";  
echo "====================================";

$IGGYMONOLITH/installNode.sh;


echo "====================================";
echo "Installing Python3";  
echo "====================================";

$IGGYMONOLITH/installPy.sh;

echo "====================================";
echo "Installing Antibody";  
echo "====================================";

$IGGYMONOLITH/installAntibody.sh;

echo "====================================";
echo "Let me Set the Aliases"; 
echo "====================================";

if [ -f $HOME/.aliases ]
then 
 rm -rf $HOME/.aliases;

echo "#Set Aliases
alias gonginx='/etc/nginx/'
alias gomysql='/etc/mysql/'
alias www='/home/'
alias zshrc='vim $HOME/.zshrc'
alias aliax='vim $HOME/.aliases'
alias vimx='vim $HOME/.vimrc'" >> $HOME/.aliases;

else
echo "#Set Aliases
alias gonginx='/etc/nginx/'
alias gomysql='/etc/mysql/'
alias www='/home/'
alias zshrc='vim $HOME/.zshrc'
alias aliax='vim $HOME/.aliases'
alias vimx='vim $HOME/.vimrc'" >> $HOME/.aliases;
fi

echo "====================================";
echo "Aliases was Set"; 
echo "====================================";

echo "====================================";
echo "Let me configure SSH to a different Port"; 
echo "====================================";

$IGGYMONOLITH/sshConfig.sh;

echo "====================================";
echo "Thank you, Now your Webserver is set and Ready"; 
echo "This is your ssh-key";
echo "====================================";
if [ -f $HOME/.ssh/id_rsa.pub ]
then
    cat $HOME/.ssh/id_rsa.pub;
else
    echo "No Legacy";
fi

if [ -f $HOME/.ssh/id_ed25519.pub ]
then
    cat $HOME/.ssh/id_ed25519.pub;
else
    echo "Modern";
fi

echo "====================================";
echo "Compleating Cycle";
sudo apt -y update;
sudo apt -y clean;
sudo apt -y autoremove;
echo "====================================";
reboot   
echo "====================================";
echo "We are the sum of our memories. Erasing the memories edged in oneself is the same as loosing oneself.";
echo "Iggy will close, Thank you Nora.";
echo "====================================";
